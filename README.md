# VG_IntersectionOfConvexPolygons


## Getting started

The code is in 3 files:

- Input.json - file containing definiton of all polygons P, Q.
- main.py - main file of program. Run by "python main.py" (needs Input.json in same folder to work)
- main.exe - compiled version of code. Doesn't need python to run. (needs Input.json in same folder to work)

## Controling the application

After opening the app a window will appear. There u can iterace the algorithm by clicking a space button (mouse needs to be in the window for it to work) one step at the time.

Left side containts current values of algorithm during that step as well as current 2 polygons. Those can be changed using the window on top. Application automaticly loads all polygons from Input.json.

Right side represents the algorithm output, polygon P,Q, current selected line of each algorithm and output (black) lines.
