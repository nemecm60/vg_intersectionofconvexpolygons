# Copyright (c) 2021 Ladislav Čmolík
#
# Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is
# hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
# FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
# OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
# OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import sys, random, math

from PySide6 import QtCore
from PySide6.QtCore import Qt, QSize
from PySide6.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QSizePolicy, QHBoxLayout, \
    QTextBrowser, QWidget, QVBoxLayout, QSpinBox
from PySide6.QtOpenGLWidgets import QOpenGLWidget
from PySide6.QtGui import QBrush, QPen, QTransform, QPainter, QSurfaceFormat, QColor, QFont
import numpy as np
import json
from enum import Enum


class INFLAG(Enum):
    PIN = "Pin"
    QIN = "Qin"
    UNKNOWN = "Unknown"


class ADVANCE(Enum):
    P = "Advanced P"
    Q = "Advanced Q"
    NONE = "None advanced"


class INTERSECT(Enum):
    PARALLEL = "Parallel"
    INTERSECTION = "Intersection"
    NO_INTERSECTION = "No intersection"
    OVERLAP = "Line are collinear"


class VisGraphicsScene(QGraphicsScene):
    def __init__(self):
        super(VisGraphicsScene, self).__init__()
        self.penP = QPen(QColor(255, 0, 0))
        self.penP.setWidth(5)

        self.penQ = QPen(QColor(0, 0, 255))
        self.penQ.setWidth(5)

        self.penO = QPen(Qt.black)
        self.penO.setWidth(6)

        self.penSelectedP = QPen(QColor(155, 0, 0))
        self.penSelectedPointP = QPen(QColor(155, 0, 0))
        self.penSelectedP.setWidth(8)
        self.penSelectedPointP.setWidth(20)

        self.penSelectedQ = QPen(QColor(0, 0, 155))
        self.penSelectedPointQ = QPen(QColor(0, 0, 155))
        self.penSelectedQ.setWidth(8)
        self.penSelectedPointQ.setWidth(20)


class VisGraphicsView(QGraphicsView):
    def __init__(self, scene, parent):
        super(VisGraphicsView, self).__init__(scene, parent)
        self.startX = 0.0
        self.startY = 0.0
        self.distance = 0.0
        self.myScene = scene
        self.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)

    def wheelEvent(self, event):
        zoom = 1 + event.angleDelta().y() * 0.001
        self.scale(zoom, zoom)

    def mousePressEvent(self, event):
        self.startX = event.position().x()
        self.startY = event.position().y()
        self.myScene.wasDragg = False
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        endX = event.position().x()
        endY = event.position().y()
        deltaX = endX - self.startX
        deltaY = endY - self.startY
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY)
        if (distance > 5):
            self.myScene.wasDragg = True
        super().mouseReleaseEvent(event)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.input = [[[], []]]

        with open("input.json") as f:
            self.input = json.load(f)

        self.P = self.input[0][0]
        self.Q = self.input[0][1]

        self.PPoints = []
        self.PEdges = []
        self.QPoints = []
        self.QEdges = []
        self.outPoints = []
        self.outEdges = []

        self.a = 0
        self.b = 0
        self.a_prev = len(self.P) - 1
        self.b_prev = len(self.Q) - 1
        self.aa = 0
        self.bb = 0
        self.inflag = INFLAG.UNKNOWN
        self.firstPoint = True
        self.output = []
        self.advanced = ADVANCE.NONE
        self.error = ""
        self.stop = False

        self.cross = 0
        self.aHB = 0
        self.bHA = 0
        self.code = INTERSECT.NO_INTERSECTION

        self.setWindowTitle('Intersection of convex polygons')
        self.createGraphicView()
        self.generateAndMapData()
        # self.setMinimumSize(800, 600)
        self.show()

    def resetValues(self):
        self.PPoints = []
        self.PEdges = []
        self.QPoints = []
        self.QEdges = []
        self.outPoints = []
        self.outEdges = []

        self.a = 0
        self.b = 0
        self.a_prev = len(self.P) - 1
        self.b_prev = len(self.Q) - 1
        self.aa = 0
        self.bb = 0
        self.inflag = INFLAG.UNKNOWN
        self.firstPoint = True
        self.output = []
        self.advanced = ADVANCE.NONE
        self.error = ""
        self.stop = False

        self.cross = 0
        self.aHB = 0
        self.bHA = 0
        self.code = INTERSECT.NO_INTERSECTION

    def createGraphicView(self):
        self.scene = VisGraphicsScene()
        self.brush = [QBrush(Qt.yellow), QBrush(Qt.green), QBrush(Qt.blue)]

        format = QSurfaceFormat()
        format.setSamples(4)

        gl = QOpenGLWidget()
        gl.setFormat(format)
        gl.setAutoFillBackground(True)

        self.view = VisGraphicsView(self.scene, self)
        self.view.setViewport(gl)
        self.view.setBackgroundBrush(QColor(255, 255, 255))
        self.view.scale(1, -1)

        self.textWidget = QTextBrowser()
        self.textWidget.setFont(QFont('Arial', 30))
        self.spinbox = QSpinBox()
        self.spinbox.setMinimumHeight(40)
        self.spinbox.valueChanged.connect(self.spinboxChange)

        self.spinbox.setMinimum(0)
        self.spinbox.setMaximum(len(self.input) - 1)

        self.vbox = QVBoxLayout(self)
        self.vbox.addWidget(self.spinbox)
        self.vbox.addWidget(self.textWidget)

        self.hbox = QHBoxLayout(self)
        self.hbox.addLayout(self.vbox)
        self.hbox.addWidget(self.view)

        wdt = QWidget()
        wdt.setLayout(self.hbox)

        self.setCentralWidget(wdt)
        self.view.setGeometry(0, 0, 800, 600)

    def generateAndMapData(self):

        for m in range(len(self.P)):
            self.PPoints.append(self.scene.addEllipse(self.P[m][0], self.P[m][1], 0.1, 0.1, self.scene.penP))
            self.PEdges.append(
                self.scene.addLine(self.P[m][0], self.P[m][1], self.P[(m - 1) % len(self.P)][0],
                                   self.P[(m - 1) % len(self.P)][1],
                                   self.scene.penP))

        for n in range(len(self.Q)):
            self.QPoints.append(self.scene.addEllipse(self.Q[n][0], self.Q[n][1], 0.1, 0.1, self.scene.penQ))
            self.QEdges.append(
                self.scene.addLine(self.Q[n][0], self.Q[n][1], self.Q[(n - 1) % len(self.Q)][0],
                                   self.Q[(n - 1) % len(self.Q)][1], self.scene.penQ))

        self.setWidgetText()

    def predicate(self, a, b, c):
        '''
            LEFT = 1
            RIGHT = -1
            STRAIGHT = 0
        '''
        return a[0] * b[1] + b[0] * c[1] + c[0] * a[1] - a[0] * c[1] - b[0] * a[1] - c[0] * b[1]

    def addPoint(self, point):

        self.addedPoint = point

        self.output.append(point)
        self.outPoints.append(self.scene.addEllipse(point[0], point[1], 0.1, 0.1, self.scene.penO))
        if len(self.output) > 1:
            self.outEdges.append(
                self.scene.addLine(self.output[-2][0], self.output[-2][1], self.output[-1][0], self.output[-1][1],
                                   self.scene.penO))

    def intersect(self, a0, a1, b0, b1):

        denom = (a0[0] - a1[0]) * (b0[1] - b1[1]) - (a0[1] - a1[1]) * (b0[0] - b1[0])
        if denom == 0:
            # lines paralel and intersect
            if self.predicate(a0, a1, b0) == 0:
                if (min(a0[0], a1[0]) <= max(b0[0], b1[0]) and
                        min(b0[0], b1[0]) <= max(a0[0], a1[0]) and
                        min(a0[1], a1[1]) <= max(b0[1], b1[1]) and
                        min(b0[1], b1[1]) <= max(a0[1], a1[1])):
                    return [INTERSECT.OVERLAP, [0, 0]]
            return [INTERSECT.PARALLEL, [0, 0]]

        t = ((a0[0] - b0[0]) * (b0[1] - b1[1]) - (a0[1] - b0[1]) * (b0[0] - b1[0])) / denom
        u = ((a0[0] - b0[0]) * (a0[1] - a1[1]) - (a0[1] - b0[1]) * (a0[0] - a1[0])) / denom
        if 0 <= t <= 1 and 0 <= u <= 1:
            return [INTERSECT.INTERSECTION, [a0[i] + (a1[i] - a0[i]) * t for i in range(2)]]
        else:
            return [INTERSECT.NO_INTERSECTION, [0, 0]]

    def inOut(self, point):
        self.addPoint(point)

        if self.aHB > 0:
            return INFLAG.PIN
        elif self.bHA > 0:
            return INFLAG.QIN
        else:
            return self.inflag

    def algStep(self):

        if not ((self.aa <= len(self.P) or self.bb <= len(self.Q)) and (
                self.aa <= 2 * len(self.P)) and self.bb <= 2 * len(self.Q) and not self.stop):

            if self.inflag == INFLAG.UNKNOWN and not self.stop:
                self.error = "One polygon is inside the other"
            return

        self.error = ""

        self.a_prev = (self.a - 1) % len(self.P)
        self.b_prev = (self.b - 1) % len(self.Q)

        self.PEdges[self.a_prev].setPen(self.scene.penP)
        self.PEdges[self.a].setPen(self.scene.penSelectedP)
        self.PPoints[self.a_prev].setPen(self.scene.penP)
        self.PPoints[self.a].setPen(self.scene.penSelectedPointP)
        self.QEdges[self.b_prev].setPen(self.scene.penQ)
        self.QEdges[self.b].setPen(self.scene.penSelectedQ)
        self.QPoints[self.b_prev].setPen(self.scene.penQ)
        self.QPoints[self.b].setPen(self.scene.penSelectedPointQ)

        A = [self.P[self.a][i] - self.P[self.a_prev][i] for i in range(2)]
        B = [self.Q[self.b][i] - self.Q[self.b_prev][i] for i in range(2)]

        self.cross = self.predicate([0, 0], A, B)
        self.aHB = self.predicate(self.Q[self.b_prev], self.Q[self.b], self.P[self.a])
        self.bHA = self.predicate(self.P[self.a_prev], self.P[self.a], self.Q[self.b])
        self.code, point = self.intersect(self.P[self.a], self.P[self.a_prev], self.Q[self.b], self.Q[self.b_prev])

        # if A & B intersects
        if self.code == INTERSECT.INTERSECTION:
            if self.inflag == INFLAG.UNKNOWN and self.firstPoint:
                self.firstPoint = False
                self.aa = 0
                self.bb = 0
            self.inflag = self.inOut(point)

        # special
        if self.code == INTERSECT.OVERLAP and A[0] * B[0] + A[1] * B[1] < 0:
            self.error = "Opposite oriented - Output is intersection of these lines"
            self.stop = True
        if self.cross == 0 and self.aHB < 0 and self.bHA < 0:
            self.stop = True
            self.error = "Parallel and separated"
        # collinear - advance but don't print anything
        elif self.cross == 0 and self.aHB == 0 and self.bHA == 0:
            if self.inflag == INFLAG.PIN:
                self.b = (self.b + 1) % len(self.Q)
                self.bb += 1
                self.advanced = ADVANCE.Q
            else:
                self.a = (self.a + 1) % len(self.P)
                self.aa += 1
                self.advanced = ADVANCE.P

        # advance
        elif self.cross >= 0:
            if self.bHA > 0:
                if self.inflag == INFLAG.PIN: self.addPoint(self.P[self.a])
                self.a = (self.a + 1) % len(self.P)
                self.aa += 1
                self.advanced = ADVANCE.P
            else:
                if self.inflag == INFLAG.QIN: self.addPoint(self.Q[self.b])
                self.b = (self.b + 1) % len(self.Q)
                self.bb += 1
                self.advanced = ADVANCE.Q
        else:
            if self.aHB > 0:
                if self.inflag == INFLAG.QIN: self.addPoint(self.Q[self.b])
                self.b = (self.b + 1) % len(self.Q)
                self.bb += 1
                self.advanced = ADVANCE.Q

            else:
                if self.inflag == INFLAG.PIN: self.addPoint(self.P[self.a])
                self.a = (self.a + 1) % len(self.P)
                self.aa += 1
                self.advanced = ADVANCE.P

    def clearGraph(self):
        for i in self.PPoints:
            self.scene.removeItem(i)

        for i in self.PEdges:
            self.scene.removeItem(i)

        for i in self.QPoints:
            self.scene.removeItem(i)

        for i in self.QEdges:
            self.scene.removeItem(i)

        for i in self.outPoints:
            self.scene.removeItem(i)

        for i in self.outEdges:
            self.scene.removeItem(i)

        self.resetValues()

    def spinboxChange(self):
        self.clearGraph()
        self.P = self.input[self.spinbox.value()][0]
        self.Q = self.input[self.spinbox.value()][1]
        self.generateAndMapData()

    def setWidgetText(self):
        self.textWidget.setText(
            " P - Red,   Q - Blue \n\n" +
            "Cross: " + str(np.sign(self.cross)) + "\n" +
            "aHB: " + str(np.sign(self.aHB)) + "\n" +
            "bHA: " + str(np.sign(self.bHA)) + "\n" +
            "Inflag " + str(self.inflag.value) + "\n" +
            str(self.code.value) + "\n" +
            str(self.advanced.value) + "\n" +
            "Error: " + str(self.error)
        )

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key.Key_Space:
            self.algStep()
            self.setWidgetText()


def main():
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
